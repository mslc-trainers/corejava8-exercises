package com.mslc.training.java8.newfeatures;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.time.LocalDateTime;
import java.util.Date;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.mslc.training.java8.ch10.template.Employee;

public class NashornExamples {

	public static void main(String[] args) throws ScriptException, FileNotFoundException, NoSuchMethodException {

		ScriptEngine engine = new ScriptEngineManager().getEngineByName("nashorn");
		engine.eval("print(\"hello world\")");

		System.out.println(" ---- ");

		ClassLoader classLoader = NashornExamples.class.getClassLoader();
		File file = new File(classLoader.getResource("my-script.js").getFile());
		
		engine.eval(new FileReader(file));
		
		Invocable invocable = (Invocable) engine;

		Object result = invocable.invokeFunction("printName","Bank of America....");

		
		System.out.println(result);
		System.out.println(result.getClass());
		

		invocable.invokeFunction("printJavaObject", new Date());
		// [object java.util.Date]
		

		

		invocable.invokeFunction("printJavaObject", LocalDateTime.now());
		// [object java.time.LocalDateTime]

		invocable.invokeFunction("printJavaObject", new Employee());
		
		invocable.invokeFunction("callingJavaFunction");


	}

	public static String javaFunction(String name) {
		System.out.format("Hi there from Java, %s", name);
		return "greetings from java";
	}

}

















