package com.mslc.training.java8.boa;

import java.util.concurrent.locks.StampedLock;

public class StampedLockDemo2 {

	public static void main(String[] args) {

		MyDataStructure ds = new MyDataStructure();

		new Thread() {

			@Override
			public void run() {

				ds.readValues();
			}
		}.start();

		new Thread() {

			@Override
			public void run() {

				try {
					Thread.sleep(6000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ds.setValues();
			}
		}.start();

	}

}

class MyDataStructure {

	StampedLock l = new StampedLock();

	int i = 0;

	public void setValues() {

		System.out.println("Trying to acquire  write lock");
		long stamp = l.writeLock();
		System.out.println("Got write lock");

		i = 5;
		l.unlock(stamp);

	}

	public void readValues() {

		long stamp = l.tryOptimisticRead();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int localValue = i;
		if (l.validate(stamp)) {
			System.out.println("Success Reading : " + localValue);
		} else {
			System.out.println("Invalid...");
		}

		// l.unlock(stamp);

	}

}
