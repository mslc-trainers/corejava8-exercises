package com.mslc.training.java8.boa;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeDemo {

	public static void main(String[] args) {

		Date d = new Date(1000L);
//		TimeZone.setDefault(TimeZone.getDefault().getTimeZone("GMT"));
		System.out.println(d);

		LocalDate d1 = LocalDate.of(2019, 11, 29);

		System.out.println(d1);

		LocalDateTime d2 = LocalDateTime.now();
		System.out.println(d2);

		LocalTime t = LocalTime.now();
		System.out.println(t);

		ZonedDateTime d4 = ZonedDateTime.now(ZoneId.of("Europe/London"));
		System.out.println(d4);

//		ZoneId.getAvailableZoneIds().forEach(System.out::println);

		String dateFormat = "dd-MM-yyyy hh:mm:ss a";

		String dateInString = "22-01-2015 10:15:55 AM";
//		LocalDateTime ldt =  LocalDateTime.now();
		LocalDateTime parsedDate = LocalDateTime.parse(dateInString, DateTimeFormatter.ofPattern(dateFormat));
		System.out.println(parsedDate);

		ZonedDateTime zdt = ZonedDateTime.of(parsedDate, ZoneId.of("Asia/Kolkata"));
		ZonedDateTime inLondon = zdt.withZoneSameInstant(ZoneId.of("Europe/London"));

		System.out.println(zdt + " -- " + inLondon);

	}

}
