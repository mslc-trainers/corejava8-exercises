//package com.mslc.training.java8.boa;
//
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//import java.util.concurrent.locks.Condition;
//import java.util.concurrent.locks.Lock;
//import java.util.concurrent.locks.ReadWriteLock;
//import java.util.concurrent.locks.ReentrantLock;
//import java.util.concurrent.locks.ReentrantReadWriteLock;
//import java.util.concurrent.locks.StampedLock;
//
//public class LockAPIDemo {
//
//	public static void main(String[] args) {
//
//		StampedLock l = null;
//
//		MyDataStructure ds = new MyDataStructure(3);
//		ds.put("v1");
//		ds.put("v2");
//		ds.put("v3");
//		ds.put("v4");
//
//	}
//
//}
//
//class MyDataStructure2 {
//
//	ReadWriteLock rwLock = new ReentrantReadWriteLock();
//	Lock rLock = rwLock.readLock();
//	Lock wLock = rwLock.writeLock();
//
//	Lock lock = new ReentrantLock();
//	Condition fullCondition = lock.newCondition();
//
//	private List<String> values;
//	private int size;
//
//	public MyDataStructure2(int size) {
//		this.size = size;
//
//	}
//
//	public void put(String value) {
//
//		try {
//			lock.lock();
//
//			if (values.size() == size) {
//				try {
//					fullCondition.await();
//
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//
//			}
//			values.add(value);
//		} finally {
//			lock.unlock();
//		}
//	}
//
//	public String get(int index) {
//
//		return values.get(index);
//	}
//
//	public String take() {
//		lock.lock();
//		try {
//			String value = values.remove(0);
//			fullCondition.signal();
//			return value;
//		} finally {
//			lock.unlock();
//		}
//
//	}
//
//}
