package com.mslc.training.java8.boa;

import java.util.function.BiFunction;
import java.util.function.Predicate;

public class Ch4App0MethodReferences {

	public static void main(String[] args) {
		
	}
}

class MethodReferenceDemo {
	
	public void someHigherOrderFunction2(Predicate<Integer> p) {
		System.out.println("In someHigherOrderFunction : ");
		
		System.out.println(p.test(5));

	}
	
	public static boolean demo2(Integer value) {
		System.out.println("in demo 2 :" + value);
		if (value > 5) {
			return true;
		} else {
			return false;
		}
		
		//return value2 + " -- New value";

	}

	
	

	public void someHigherOrderFunction(BiFunction<String, String, Integer> f) {
		System.out.println("In someHigherOrderFunction : ");
		
		Integer i = f.apply("v1", "v2");
		
		

	}

	public static Integer demo1(String value1, String value2) {
		System.out.println("in demo 1 : " + value1 + " -- " + value2);
		//return value2 + " -- New value";
		
		return null;

	}

}

class HealthInsuranceService {

	private String id;

	HealthInsuranceService() {

	}

	HealthInsuranceService(String id) {
		System.out.println("Construct 2 executed : " + id);
		this.id = id;
	}

	@Override
	public String toString() {

		return id;
	}

	public static HealthInsuranceService addNewInsurance(HealthInsuranceService service, String insuranceName) {

		System.out.println("in addNewInsurance : " + insuranceName + " -- " + service);

		return service;
	}

	public HealthInsuranceService activateInsurance(HealthInsuranceService service, String insuranceName) {

		System.out.println("in activateInsurance : " + insuranceName + " -- " + service);

		return service;
	}

	public String deactivateInsurance(String insuranceName) {

		System.out.println("in deactivateInsurance : " + insuranceName + " -- " + this);
		return "this";
	}

}

interface MyComparator<T> {

	int cmp(T o1, T o2);
}
