package com.mslc.training.java8.boa;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Demo2 {

	public static void main(String[] args) {

		BiConsumer<HealthInsuranceService2, String> staticReference = null;
		
		

		List<String> names = Arrays.asList("12", "14");

		names.forEach(System.out::println);

		Consumer<String> c = System.out::println;

		Predicate<Integer> p = x -> x > 5;

		Stream<Integer> s = Stream.of(1, 2, 5);
		s.filter(x -> x > 5);

		Predicate<Integer> p1 = new Predicate<Integer>() {

			@Override
			public boolean test(Integer t) {

				return t > 5;
			}
		};

		p.test(4);
		p1.test(5);
	}

}

class HealthInsuranceService2 {

	private String id;

	HealthInsuranceService2() {

	}

	HealthInsuranceService2(String id) {
		System.out.println("Construct 2 executed : " + id);
		this.id = id;
	}

	@Override
	public String toString() {

		return id;
	}

	public static HealthInsuranceService addNewInsurance(HealthInsuranceService service, String insuranceName) {

		System.out.println("in addNewInsurance : " + insuranceName + " -- " + service);

		return service;
	}

	public HealthInsuranceService activateInsurance(HealthInsuranceService service, String insuranceName) {

		System.out.println("in activateInsurance : " + insuranceName + " -- " + service);

		return service;
	}

	public String deactivateInsurance(String insuranceName) {

		System.out.println("in deactivateInsurance : " + insuranceName + " -- " + this);
		return "this";
	}

}
