package com.mslc.training.java8.boa;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveAction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ForkJoinPoolDemo {

	public static void main(String[] args) {

		String arr[] = new String[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" };

		ForkJoinPool fjp = new ForkJoinPool();
		
		
		MyTask t = new MyTask(arr);
		
		
		fjp.execute(t);
		t.join();
		

	}

}

class MyTask extends RecursiveAction {

	private String[] arr;
	private static final int THRESHOLD = 3;

	public MyTask(String arr[]) {

		this.arr = arr;
	}

	@Override
	protected void compute() {
		
		if (arr.length > THRESHOLD) {
			
			MyTask task1 = new MyTask(Arrays.copyOfRange(arr, 0, arr.length / 2));
			MyTask task2 = new MyTask(Arrays.copyOfRange(arr, arr.length / 2, arr.length));
			
			ForkJoinTask.invokeAll(Arrays.asList(task1, task2));
			
			
		} else {
			
			String s = Stream.of(arr)
			   .collect(Collectors.joining());
			
			System.out.println(s);
		}
		
		
	}

}
