package com.mslc.training.java8.boa;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

import com.mslc.training.java8.model.Employee;

public class ReductionDemo {

	public static void main(String[] args) {
		
		List<Employee> emps = Arrays.asList(new Employee("John", 30L),new Employee("Mike", 40L),  new Employee("Julie", 35L));
		
		BiFunction<Long, Employee, Long> accumulator = (salary, emp) -> salary + emp.getSalary();
		BinaryOperator<Long> combiner = (x, y) -> x + y;

		Long result = emps
				.stream()
				.parallel()
				.reduce(0L, 
						accumulator, combiner
						);
		 System.out.println(result);
		 
		 
		 BiFunction<String, Employee, String> accumulator2 = (name, emp) -> name + emp.getName();
			BinaryOperator<String> combiner2 = (x, y) -> x + y;

			String result2 = emps
					.stream()
					.parallel()
					.reduce(new String(), 
							accumulator2, combiner2
							);
			 System.out.println(result2);
			 
		 
 
//		 String s = users
//				  .stream()
//				  
//				  .parallel()
//				  .collect(StringBuilder::new,
//	                 (sb, s1) -> sb.append(s1.getAge()),
//	                 (sb1, sb2) -> sb1.append(sb2))
//				  .toString();
//		 
//		 System.out.println(s);
		 
		 
	
				  
				  
		 
		 
			 
			 
		 if (true) {
			 return;
		 }

		List<Integer> ints = Arrays.asList(1, 2, 4, 5);

		int sum = 10;
		for (Integer i : ints) {
			sum = sum + i;
		}
		System.out.println(sum);

		Optional<Integer> i = ints.stream().reduce((x, y) -> x + y);

		System.out.println(i.get());

		// @formatter:off

		Integer value = ints
				   .stream()
				   .reduce(10, (x, y) -> x + y);
		 
		// @formatter:on

		System.out.println(value);


		

	}

	public static void someFunction(String name) {

		Objects.requireNonNull(name);

	}

}
