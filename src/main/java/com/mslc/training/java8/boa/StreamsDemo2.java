package com.mslc.training.java8.boa;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class StreamsDemo2 {

	public static void main(String[] args) {

		List<String> names = Arrays.asList("BoA", "Nomura", "Morgan Stanley", "JPMC", "Barclays");
		
		/**
		 * Print each element in filter and map
		 * Print the thread in which it is executing
		 * Use parallelStream and make note of the threads. 
		 * 
		 */

		// @formatter:off

		Set<String> allNames =  names
		  .parallelStream()		  
		  .filter(x -> {
			  
//			  System.out.println("In filter : "  + x + " -- " + Thread.currentThread().getName());
			  return true;
//			  return x.startsWith("Morgan");
		  })
		  .sorted()
		  .map(x -> {
			
//			  System.out.println("in map : " + x + " -- " + Thread.currentThread().getName());
			  return x.toLowerCase();
		  })
		  .collect(Collectors.toCollection(TreeSet::new));
		 
		// @formatter:on
		
		System.out.println(allNames);
		System.out.println("main ends..");
		
		

		// @formatter:on

	}

}
