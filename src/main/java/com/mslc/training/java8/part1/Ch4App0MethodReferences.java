package com.mslc.training.java8.part1;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

import com.mslc.training.java8.model.Employee;
import com.mslc.training.java8.model.HealthData;

public class Ch4App0MethodReferences {

	public static void main(String[] args) {

	}

}

class EmployeeProcessor {

	public void process(BiConsumer<Employee, String> f) {

		List<Employee> employees = HealthData.employeeList;
		System.out.println(" --- -");
		for (Employee emp : employees) {
			System.out.print(emp.hashCode() + " - ");
			f.accept(emp, emp.getLastName());
		}
		System.out.println(" --- -");
	}

}

class HealthInsuranceService {

	private String id;

	HealthInsuranceService() {

	}

	HealthInsuranceService(String id) {
		System.out.println("Construct 2 executed : " + id);
		this.id = id;
	}

	@Override
	public String toString() {

		return id;
	}

	public static HealthInsuranceService addNewInsurance(HealthInsuranceService service, String insuranceName) {

		System.out.println("in addNewInsurance : " + insuranceName + " -- " + service);

		return service;
	}

	public HealthInsuranceService activateInsurance(HealthInsuranceService service, String insuranceName) {

		System.out.println("in activateInsurance : " + insuranceName + " -- " + service);

		return service;
	}

	public String deactivateInsurance(String insuranceName) {

		System.out.println("in deactivateInsurance : " + insuranceName + " -- " + this);
		return "this";
	}

}

interface MyComparator<T> {

	int cmp(T o1, T o2);
}
