package com.mslc.training.java8.part1;

import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.DoubleBinaryOperator;
import java.util.function.DoubleUnaryOperator;
import java.util.function.Function;
import java.util.function.IntBinaryOperator;
import java.util.function.IntUnaryOperator;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToIntBiFunction;
import java.util.function.ToIntFunction;
import java.util.function.UnaryOperator;

import com.mslc.training.java8.model.Employee;

public class Ch1App1OOTBFunctionalInterfaces {

	public static void main(String[] args) {

		Supplier<Employee> s = () -> {

			return new Employee("asd");

		};

		Function<Integer, Integer> adder = x -> x + 1;

		BiFunction<Integer, String, String> biFunction = (x, y) -> x + " -- " + y;

		Predicate<String> predicate = x -> x.length() > 5;

		UnaryOperator<String> uo = x -> x.length() + "5";

		BinaryOperator<Integer> bo = (x, y) -> x + y;

		IntUnaryOperator iuo = x -> x + 5;

		DoubleUnaryOperator duo;

		IntBinaryOperator ibo = (x, y) -> x + y;

		DoubleBinaryOperator dbo;

		ToIntFunction<String> tif;
		ToIntBiFunction<Employee, String> tibf;

	}
}
