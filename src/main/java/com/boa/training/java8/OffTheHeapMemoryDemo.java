//package com.boa.training.java8;
//
//import java.nio.ByteBuffer;
//
//public class OffTheHeapMemoryDemo {
//
//	public static void main(String[] args) throws InterruptedException {
//
//		ByteBuffer b = ByteBuffer.allocateDirect(300 * 1024 * 1024);
//		System.out.println("Memory Allocated...");
//		Thread.sleep(Long.MAX_VALUE);
//
//	}
//}
