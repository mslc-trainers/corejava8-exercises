package com.boa.training.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.mslc.training.java8.model.Employee;
import com.mslc.training.java8.model.HealthData;

public class FunctionalProgrammingBenefitsDemo {

	public static void main(String[] args) {

		List<Employee> empList = HealthData.employeeList;
		
		List<String> empNames  = mapEmployeeObjectsJava8Way(empList, x -> x.getName());
		System.out.println(empNames);
		
		List<String> empNames1  = mapEmployeeObjectsJava8Way(empList, x -> x.getName() + " -- " + x.getAge());

		System.out.println(empNames1);

		
//		List<String> empNames = mapEmployeeObjects(empList);
		
		
		
	}
	
	/**
	 * Java 1.7 way
	 */
	
	public static List<String> mapEmployeeObjects(List<Employee> emps) {
		List<String> names = new ArrayList<String>();
		for (Employee emp : emps) {
			String name = emp.getName();
			names.add(name);
		}
		
		return names;
	}
	
	public static List<String>
	    mapEmployeeObjectsJava8Way(List<Employee> emps, Function<Employee, String> mapper) {
		
		
		List<String> names = new ArrayList<String>();
		for (Employee emp : emps) {
			
			String value = mapper.apply(emp);
			names.add(value);
			
//			String name = emp.getName();
//			names.add(name);
		}
		
		return names;
	}

}





