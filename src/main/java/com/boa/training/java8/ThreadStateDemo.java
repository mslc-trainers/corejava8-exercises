//package com.boa.training.java8;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class ThreadStateDemo {
//
//	public static void main1(String[] args) {
//
//		Integer age1 = 250;
//		Integer age2 = 250;
//		System.out.println(age1 == age2);
//
//	}
//
//	public static void main(String[] args) throws InterruptedException {
//
////		MyInteger i = new MyInteger();
//		
//		MyBoundedDataStructure ds = new MyBoundedDataStructure(3);
//		ds.put("v1");
//		ds.put("v2");
//		ds.put("v3");
//		
//
//		Thread t1 = new Thread() {
//
//			@Override
//			public void run() {
//				ds.put("v4");
//
////				i.setValue(50);
//
//			}
//		};
//
//		Thread t2 = new Thread() {
//
//			@Override
//			public void run() {
//
//				
//				try {
//					Thread.sleep(10000);
//				} catch (InterruptedException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//				ds.remove();
//				
////				System.out.println(i.getValue());
//
//			}
//		};
//
//		t1.start();
//		Thread.sleep(100);
//		t2.start();
//
//		new Thread() {
//			public void run() {
//
//				while (true) {
//					try {
//						Thread.sleep(1000);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					System.out.println("T1 is in : " + t1.getState().name() + " state");
//				}
//
//			};
//		}.start();
//
//		System.out.println("Main thread completed.");
//	}
//
//}
//
//class MyBoundedDataStructure {
//
//	private List<String> values = new ArrayList<String>();
//	private int size;
//
//	public MyBoundedDataStructure(int size) {
//		this.size = size;
//
//	}
//
//	public synchronized void put(String value) {
//		if (values.size() == size) {
//			try {
//				this.wait();
//			} catch (InterruptedException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		values.add(value);
//
//	}
//	
//	public synchronized String remove() {
//		String value = values.remove(0);
//		this.notify();
//		return value;
//	}
//	
//	
//
//}
//
//class MyInteger {
//
//	private int value;
//
//	public synchronized void setValue(int value) {
//
//		this.value = value;
//		try {
//			Thread.sleep(10000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}
//
//	public synchronized int getValue() {
//		return this.value;
//	}
//
//}
